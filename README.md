# Load testing with locust spike

## Fulltext provider
The parameters for the load test are set in the ```locust.props``` file. The test uses the named CSV file containing the DOIs and hits 
the provider endpoint with a random DOI and a random fragment. The weightings for the fragment requests are defined in ```endpoints.py```.

To launch a container with the load-test for a service, run:
```./fig-env bash -c './launchLocust.sh fulltext-provider/'```
