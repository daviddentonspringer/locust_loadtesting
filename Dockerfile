FROM springersbm/fig-env

RUN apt-get update
RUN apt-get install -y python-dev python-pip

RUN pip install locustio
RUN pip install pyzmq
