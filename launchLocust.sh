#!/bin/sh
cd $1
. ./locust.props
locust -f ./locustfile.py --print-stats --host=$HOST --only-summary --no-web  --loglevel=INFO --clients=$CLIENT_COUNT --hatch-rate=$CLIENT_COUNT --num-request=$REQUEST_COUNT