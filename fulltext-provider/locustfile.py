from locust import HttpLocust, events

import endpoints
import output
import logging

# turn off annoying INFO logging in connection pool - we need the INFO level to display the output stats
logging.getLogger('urllib3.connectionpool').setLevel('WARN')

class ProviderClient(HttpLocust):
    task_set = endpoints.MainEndpoint

events.quitting += output.SaveStats().on_quit
