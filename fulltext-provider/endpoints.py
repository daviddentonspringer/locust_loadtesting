from locust import TaskSet, task

import random
import urllib
import os

doiFileName = os.environ['DOI_FILE']

class FileBasedDois():
    def __init__(self, filename):
        self.dois = open(filename).read().strip().split('\n')

    def doi(self):
        return urllib.quote_plus(random.choice(self.dois))


class MainEndpoint(TaskSet):
    def on_start(self):
        self.dois = FileBasedDois(doiFileName)

    def __request(self, fragment_name):
        self.client.get('/%s/%s' % (self.dois.doi(), fragment_name), name=fragment_name)

    @task(5)
    def abstract(self):
        self.__request('abstract')

    @task(2)
    def excerpt(self):
        self.__request('excerpt')

    @task(4)
    def references(self):
        self.__request('references')

    @task(10)
    def body(self):
        self.__request('body')